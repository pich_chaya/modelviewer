
// ModelViewerView.h : interface of the CModelViewerView class
//

#pragma once
#include "OpenGLPickView.h"
#include "ModelDataStructure.h"
class CModelViewerView : public COpenGLPickView
{
//member variable
private:
	BOOL	m_bSpin;
	BOOL	m_bPan;
	BOOL	m_bZoom;
	BOOL	m_bRenderPoint;
	BOOL	m_bRenderLine;
	BOOL	m_bRenderFace;
	BOOL	m_bPickPoint;
	BOOL	m_bPickLine;
	BOOL	m_bPickFace;
	BOOL	m_VertexNormal;
	BOOL	m_FaceNormal;
	BOOL	m_curvature;
	CModel	m_model;
	unsigned int m_uiDisplayListIndex_Point;
	unsigned int m_uiDisplayListIndex_Line;
	unsigned int m_uiDisplayListIndex_Face;
	unsigned int m_uiDisplayListIndex_NormalLine;
	unsigned int m_uiDisplayListIndex_FaceNormalLine;
	unsigned int m_uiDisplayListIndex_curvature;

//member function
private:
	void	DoTracking (CPoint point);
	void	RenderObjects(ERENDERMODE eRenderMode, const unsigned int* auiName);
	void	DeleteDisplayList();

protected: // create from serialization only
	CModelViewerView();
	DECLARE_DYNCREATE(CModelViewerView)

// Attributes
public:
	CModelViewerDoc* GetDocument() const;

// Operations
public:

// Overrides
public:
//	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:

// Implementation
public:
	virtual ~CModelViewerView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	afx_msg void OnFilePrintPreview();
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnViewSpin();
	afx_msg void OnUpdateViewSpin(CCmdUI *pCmdUI);
	afx_msg void OnViewPan();
	afx_msg void OnUpdateViewPan(CCmdUI *pCmdUI);
	afx_msg void OnViewZoom();
	afx_msg void OnUpdateViewZoom(CCmdUI *pCmdUI);
	afx_msg void OnViewRenderpoint();
	afx_msg void OnUpdateViewRenderpoint(CCmdUI *pCmdUI);
	afx_msg void OnViewRenderline();
	afx_msg void OnUpdateViewRenderline(CCmdUI *pCmdUI);
	afx_msg void OnViewRenderface();
	afx_msg void OnUpdateViewRenderface(CCmdUI *pCmdUI);
	afx_msg void OnViewPickpoint();
	afx_msg void OnUpdateViewPickpoint(CCmdUI *pCmdUI);
	afx_msg void OnViewPickline();
	afx_msg void OnUpdateViewPickline(CCmdUI *pCmdUI);
	afx_msg void OnViewPickface();
	afx_msg void OnUpdateViewPickface(CCmdUI *pCmdUI);
	afx_msg void OnFileOpen();
	afx_msg void OnFileNew();
	afx_msg void OnDrawingNormalvertices();
	afx_msg void OnUpdateDrawingNormalvertices(CCmdUI *pCmdUI);
	afx_msg void OnDrawingVerticesnormal();
	afx_msg void OnUpdateDrawingVerticesnormal(CCmdUI *pCmdUI);
	afx_msg void OnDrawingFacenormal();
	afx_msg void OnUpdateDrawingFacenormal(CCmdUI *pCmdUI);
	afx_msg void OnDrawingCurvature();
	afx_msg void OnUpdateDrawingCurvature(CCmdUI *pCmdUI);
};

#ifndef _DEBUG  // debug version in ModelViewerView.cpp
inline CModelViewerDoc* CModelViewerView::GetDocument() const
   { return reinterpret_cast<CModelViewerDoc*>(m_pDocument); }
#endif

