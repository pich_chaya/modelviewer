#pragma once
#include "ModelDataStructure.h" 
#include "Vec.h"
using namespace std;
#include <algorithm>
#include <math.h>


//CalclateCenterOfGravity + Volume
void CalcCenterOfGravity(CModel& model) {

	float totalVolume = 0,currentVolume;
	float xCenter = 0, yCenter = 0, zCenter = 0;

	for (int i = 0; i < model.vIndexedTriangle.size(); i++)
	{
		totalVolume += currentVolume =
			(model.vVertex[model.vIndexedTriangle[i][0]][0]
		* model.vVertex[model.vIndexedTriangle[i][1]][1]
		* model.vVertex[model.vIndexedTriangle[i][2]][2]

		- model.vVertex[model.vIndexedTriangle[i][0]][0] 
		* model.vVertex[model.vIndexedTriangle[i][2]][1]
		* model.vVertex[model.vIndexedTriangle[i][1]][2]

		- model.vVertex[model.vIndexedTriangle[i][1]][0]
		* model.vVertex[model.vIndexedTriangle[i][0]][1]
		* model.vVertex[model.vIndexedTriangle[i][2]][2]

		+ model.vVertex[model.vIndexedTriangle[i][1]][0]
		* model.vVertex[model.vIndexedTriangle[i][2]][1]
		* model.vVertex[model.vIndexedTriangle[i][0]][2]


		+ model.vVertex[model.vIndexedTriangle[i][2]][0]
		* model.vVertex[model.vIndexedTriangle[i][0]][1]
		* model.vVertex[model.vIndexedTriangle[i][1]][2]

		- model.vVertex[model.vIndexedTriangle[i][2]][0] 
		* model.vVertex[model.vIndexedTriangle[i][1]][1]
		* model.vVertex[model.vIndexedTriangle[i][0]][2])/6;

		xCenter += ((model.vVertex[model.vIndexedTriangle[i][0]][0] 
		+ model.vVertex[model.vIndexedTriangle[i][1]][0] 
		+ model.vVertex[model.vIndexedTriangle[i][2]][0]) / 4) * currentVolume;


		yCenter += ((model.vVertex[model.vIndexedTriangle[i][0]][1] 
		+ model.vVertex[model.vIndexedTriangle[i][1]][1] 
		+ model.vVertex[model.vIndexedTriangle[i][2]][1] ) / 4) * currentVolume;

		zCenter += ((model.vVertex[model.vIndexedTriangle[i][0]][2]
		+ model.vVertex[model.vIndexedTriangle[i][1]][2] 
		+ model.vVertex[model.vIndexedTriangle[i][2]][2]) / 4) * currentVolume;

	}

	model.volume = totalVolume;
	model.centerOfGravity[0] = xCenter/totalVolume;
	model.centerOfGravity[1] = yCenter/totalVolume;
	model.centerOfGravity[2] = zCenter/totalVolume;

}

//calculate area
void CalcArea(CModel& model) 
{
	float ab = 0.0f, bc = 0.0f, ac = 0.0f;
	float ax= 0.0f,ay= 0.0f,bx= 0.0f,by= 0.0f,cx= 0.0f,cy= 0.0f;
	float az=0.0f,bz=0.0f,cz=0.0f;
	float area = 0,sumArea =0,s=0;
	int count = 0;
	for (int i = 0; i < model.vIndexedTriangle.size(); i++)
	{	
		ax = model.vVertex[model.vIndexedTriangle[i][0]][0];
		ay = model.vVertex[model.vIndexedTriangle[i][0]][1];
		az = model.vVertex[model.vIndexedTriangle[i][0]][2];

		bx = model.vVertex[model.vIndexedTriangle[i][1]][0];
		by = model.vVertex[model.vIndexedTriangle[i][1]][1];
		bz = model.vVertex[model.vIndexedTriangle[i][1]][2];

		cx = model.vVertex[model.vIndexedTriangle[i][2]][0];
		cy = model.vVertex[model.vIndexedTriangle[i][2]][1];
		cz = model.vVertex[model.vIndexedTriangle[i][2]][2];

		ab = sqrt(pow(ax-bx,2)+pow(ay-by,2)+pow(az-bz,2));
		bc = sqrt(pow(bx-cx,2)+pow(by-cy,2)+pow(bz-cz,2));
		ac = sqrt(pow(ax-cx,2)+pow(ay-cy,2)+pow(az-cz,2));

		s = (ab+bc+ac)/2;
		area = sqrt(s*(s-ab)*(s-bc)*(s-ac));
		sumArea += area;
		count++;
	}
	model.totalArea = sumArea;
}

//calculate center 
void CalcCenter(CModel& model) 
{
	vec sum;
	float vertices_num = model.vVertex.size();

	for (int i = 0; i < vertices_num ; i++)
	{
		sum += model.vVertex[i];

	}

	model.center = sum/vertices_num;

}

//Calculate Curvature
void CalcCurvature(CModel& model)
{
	model.curvature.resize(model.vVertex.size());

	//find Ci 
	//Ci = ( n DOT (p-qi) ) / |p-qi|
	for(int i=0 ; i< model.vVertex.size(); i++)
	{	
		float maxCurv=-1,minCurv=1;
		for(int j = 0 ;j<model.connectivity[i].size();j++){

			for(int k=0;k<3;k++){
				if(model.vVertex[i] == model.vVertex[model.vIndexedTriangle[model.connectivity[i][j]][k]]){
					continue;
				}
				else{
					vec pq = (model.vVertex[i] - model.vVertex[model.vIndexedTriangle[model.connectivity[i][j]][k]]);
					pq = normalize(pq);
					float tempCurv = model.vVertexNormal[i] DOT pq;

					if(tempCurv > maxCurv){
						maxCurv = tempCurv;
					}
					if(tempCurv < minCurv){
						minCurv = tempCurv;
					}
					break;
				}
			}
		}

		model.curvature[i] = (maxCurv + minCurv) / 2;
		model.curvature[i] = ( model.curvature[i] + 1 ) / 2;

	}

}
