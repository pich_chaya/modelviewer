
//
// LoadOffFile.cpp
//
#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <string>
#include "ModelDataStructure.h"
#include "Vec.h"
#define	READSTRINGBUFFERLEN	256

bool LoadOffFile(const char* pszFileName, CModel& model )
{

	if( NULL == pszFileName )
	{
		return false;
	}

	unsigned int  vertices_num = 0;
	unsigned int faces_num = 0;
	unsigned int number = 0; 

	std::ifstream ifs(pszFileName);
	std::string buf;
	unsigned int i;

	if(!ifs.is_open()){
		std::cout << "cannot open "<<pszFileName << std::endl;
		return false;
	}

	while(ifs >> buf){
		if(buf =="OFF"){
			ifs >> vertices_num;
			ifs >> faces_num;
			ifs >> number;
			if(ifs.fail()){
				std::cout <<"error! vertices_num or faces_num is not int"<<std::endl;
			}
		}

		break;

	}

	if(vertices_num==0 || faces_num==0){
		return false;
	}

	std::getline(ifs,buf);
	
	model.vVertex.resize(vertices_num);
	model.vIndexedTriangle.resize(faces_num);
	model.connectivity.resize(faces_num);

	for(i= 0;i<vertices_num;i++){
		std::getline(ifs,buf);
		unsigned int length = buf.length();
		char *c = new char[length + 1];
		strcpy_s(c,length+1,buf.c_str());
		double x,y,z;
		sscanf_s(c,"%lf %lf %lf",&x,&y,&z);
		model.vVertex[i][0] = x;
		model.vVertex[i][1] = y;
		model.vVertex[i][2] = z;

	}

	for(i= 0;i<faces_num;i++){
		std::getline(ifs,buf);
		char *c = new char[buf.length() + 1];
		strcpy_s(c,buf.length()+1,buf.c_str());
		int num;
		unsigned int p[3];
		sscanf_s(c,"%d %d %d %d",&num,
			&p[0],
			&p[1],
			&p[2]);

		model.vIndexedTriangle[i][0] = p[0];
		model.vIndexedTriangle[i][1] = p[1];
		model.vIndexedTriangle[i][2] = p[2];

		model.connectivity[p[0]].push_back(i);
		model.connectivity[p[1]].push_back(i);
		model.connectivity[p[2]].push_back(i);
	}

	if( 0 == model.vVertex.size()
		|| 0 == model.vIndexedTriangle.size() )
	{
		return false;
	}

	
	
	return true;
}