
// OpenGLPickView.cpp : implementation of the COpenGLPickView class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
//#include "OpenGLPick.h"
#endif

//#include "OpenGLPickDoc.h"
#include "OpenGLPickView.h"

#include<mmsystem.h>
#pragma comment(lib,"winmm.lib")

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// COpenGLPickView

IMPLEMENT_DYNCREATE(COpenGLPickView, COpenGLTrackView)

BEGIN_MESSAGE_MAP(COpenGLPickView, COpenGLTrackView)
	ON_WM_CONTEXTMENU()
//	ON_WM_RBUTTONUP()
ON_WM_RBUTTONUP()
END_MESSAGE_MAP()

// COpenGLPickView construction/destruction

COpenGLPickView::COpenGLPickView()
{
	// TODO: add construction code here
	memset(m_auiName_pick,0x00, NAMEARRAYSIZE* sizeof(unsigned int));

}

COpenGLPickView::~COpenGLPickView()
{
}

BOOL COpenGLPickView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return COpenGLTrackView::PreCreateWindow(cs);
}

// COpenGLPickView drawing

//void COpenGLPickView::OnDraw(CDC* /*pDC*/)
//{
//	CDocument* pDoc = GetDocument();
//	ASSERT_VALID(pDoc);
//	if (!pDoc)
//		return;
//
//	// TODO: add draw code for native data here
//}

//The call of a member function is added to each message handler



void COpenGLPickView::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
//	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}


// COpenGLPickView diagnostics

#ifdef _DEBUG
void COpenGLPickView::AssertValid() const
{
	COpenGLTrackView::AssertValid();
}

void COpenGLPickView::Dump(CDumpContext& dc) const
{
	COpenGLTrackView::Dump(dc);
}

CDocument* COpenGLPickView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CDocument)));
	return (CDocument*)m_pDocument;
}
#endif //_DEBUG


// COpenGLPickView message handlers
bool COpenGLPickView::RenderScene()
{
	DWORD dwTimeStart = ::timeGetTime();

	RenderObjects(RM_RENDER,NULL);

	DWORD dwTimeStop = ::timeGetTime();
	DWORD dwLapse = dwTimeStop - dwTimeStart;

	if(0 == dwLapse)
	{
		dwLapse = 1;
	}

	CString strFps;
	strFps.Format(_T("%5d[fps] (%6.4lf[spf])"),
		(int)(1000.0 / dwLapse + 0.5),
		dwLapse/1000.0);

	CMFCStatusBar* pStatusB = (CMFCStatusBar*)AfxGetMainWnd()->GetDescendantWindow(AFX_IDW_STATUS_BAR);
	pStatusB->SetPaneText(0,strFps);
	return true;
	return true;
}

void COpenGLPickView::RenderObjects(ERENDERMODE eRenderMode, const unsigned int*auiName)
{
	int iCountPoint = 8;
	float points[8][3] = {-5,-5,-5,
						5,-5,-5,
						5,5,-5,
						-5,5,-5,
						-5,-5,5,
						5,-5,5,
						5,5,5,
						-5,5,5};
	int iCountLine = 18;
	int lines[18][2] = {0,1,
						1,2,
						2,3,
						3,0,
						4,5,
						5,6,
						6,7,
						7,4,
						0,4,
						1,5,
						2,6,
						3,7,
						1,3,
						5,2,
						4,6,
						0,7,
						2,7,
						5,0};
	int iCountTriangle = 12;	//Nember of Triangles
	int triangles[12][3] = {0,1,3,
							1,2,3,
							1,5,2,
							5,6,2,
							5,4,6,
							4,7,6,
							4,0,7,
							0,3,7,
							3,2,7,
							2,6,7,
							4,5,0,
							5,1,0};

	int iIndexTriangle;
	int iIndexLine;
	int iIndexPoint;
	switch(eRenderMode)
	{
		case RM_RENDER:
		case RM_PICK:
			::glPushAttrib(GL_ALL_ATTRIB_BITS);
			{
				//surface
				::glPushName(GL_FILL);
				::glColor3f(0.5,0.5,0);
				glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
				for(iIndexTriangle = 0; iIndexTriangle<iCountTriangle; iIndexTriangle++)
				{
					if(GL_FILL == m_auiName_pick[1] && iIndexTriangle == (int)m_auiName_pick[2])
					{
						::glPushAttrib(GL_CURRENT_BIT);
						::glColor3f(1.0,1.0,0.0);
					}
					::glPushName(iIndexTriangle);
					::glBegin(GL_TRIANGLES);
					glVertex3fv(points[triangles[iIndexTriangle][0]]);
					glVertex3fv(points[triangles[iIndexTriangle][1]]);
					glVertex3fv(points[triangles[iIndexTriangle][2]]);
					::glEnd();
					::glPopName(); //index Triangle
					if(GL_FILL == m_auiName_pick[1] && iIndexTriangle == (int)m_auiName_pick[2])
					{
						::glPopAttrib();
					}

				}

				::glPopName(); //GL_FILL
				//Line
				::glPushName(GL_LINE);
				::glLineWidth(2.0);
				::glColor3f(0.0,0.5,0.5);
				for(iIndexLine = 0; iIndexLine < iCountLine; iIndexLine++)
				{
					if(GL_LINE == m_auiName_pick[1] && iIndexLine == (int)m_auiName_pick[2])
					{
						::glPushAttrib(GL_CURRENT_BIT | GL_LINE_BIT);
						::glLineWidth(5.0);
						::glColor3f(0.0,1.0,1.0);
					}
					::glPushName(iIndexLine);
					::glBegin(GL_LINES);
						glVertex3fv(points[lines[iIndexLine][0]]);
						glVertex3fv(points[lines[iIndexLine][1]]);
					::glEnd();
					::glPopName(); //iIndexLine
					if(GL_LINE == m_auiName_pick[1] && iIndexLine == (int)m_auiName_pick[2])
					{
						::glPopAttrib();
					}

				}
				::glPopName();

				//point
				::glPushName(GL_POINT);
				::glPointSize(5.0);
				::glColor3f(0.5,0.0,0.5);
				for(iIndexPoint = 0; iIndexPoint < iCountPoint; iIndexPoint++)
				{
					if(GL_POINT == m_auiName_pick[1] && iIndexPoint == (int)m_auiName_pick[2])
					{
						::glPushAttrib(GL_CURRENT_BIT | GL_POINT_BIT);
						::glPointSize(10.0);
						::glColor3f(1.0,0.0,1.0);
					}
					::glPushName(iIndexPoint);
					::glBegin(GL_POINTS);
						glVertex3fv(points[iIndexPoint]);
					::glEnd();
					::glPopName(); //iIndexPoint
					if(GL_POINT == m_auiName_pick[1] && iIndexPoint == (int)m_auiName_pick[2])
					{
						::glPopAttrib();
					}
				}
				::glPopName();   //GL_POINT
			}
			::glPopAttrib();
			return;
		case RM_SELECT:
			{
				if(NULL == auiName)
				{
					ASSERT(!"COpenGLPickView::RenderObjects");
					return;
				}
				for(int iIndexName = 0; iIndexName < NAMEARRAYSIZE; iIndexName++)
				{
					m_auiName_pick[iIndexName] = auiName[iIndexName];
				}
				switch(auiName[1])
				{
					//Processing according to "the picked object"
					case GL_POINT:
						
					case GL_LINE:
					case GL_FILL:
						return;

					default:
							ASSERT(!"COpenGLPickView::RenderObjects");
							return;
				}
			}
			return;
		default:
			ASSERT(!"COpenGLPickView::RenderObjects");
			return;
	}
	
}

//Picking
//Dopicking() function setup "selection mode", "Name stack", and "Projection transformation matrix" for Picking. Then the object is drew

#define BUFSIZE 512
int COpenGLPickView::DoPicking(int x, int y)
{
	GLuint	uiSelectionBuffer[BUFSIZE];
	GLint	iCountHit;
	GLint	viewport[4];
	int	i = 0;

	//Get the parameter of viewport.
	//four oarameters(x,y,width,height)
	::glGetIntegerv(GL_VIEWPORT,viewport);

	//setup of buffer which used in selection mode.
	::glSelectBuffer(BUFSIZE,uiSelectionBuffer);

	(void)::glRenderMode(GL_SELECT);

	::glInitNames();
	::glPushName(0);

	::glMatrixMode(GL_PROJECTION);
	::glPushMatrix();
		::glLoadIdentity();

		//set up picking area
		::gluPickMatrix((GLdouble) x,			//X-coordinate of center point
					(GLdouble) -y+m_cy,		//Y-coordinate of center point
					10,						//width
					10,						//height
					viewport);				//parameter of viewport

		SetupViewingFrustum();

		::glMatrixMode(GL_MODELVIEW);
		RenderObjects(RM_PICK, NULL);

	::glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	::glFlush();
	::glMatrixMode(GL_MODELVIEW);

	iCountHit = ::glRenderMode(GL_RENDER);

	if(iCountHit<= 0)
	{
		//hit becomes negative when it cannot store in the buffer prepare by glSelectBuffer()
		return iCountHit;
	}

	unsigned int uiNameArray[NAMEARRAYSIZE];
	SelectionBuffer2NameArray(iCountHit, uiSelectionBuffer, uiNameArray);
	RenderObjects(RM_SELECT, uiNameArray);
	return iCountHit;

}

/////////////////////////////////////////////
//Get the information of "picked object"
void COpenGLPickView::SelectionBuffer2NameArray(GLuint uiCountHit, 
												GLuint uiSelectionBuffer[], 
												unsigned int uiNameArray_dest[NAMEARRAYSIZE])
{
	int  iIndexName;
	GLuint ui;
	GLuint *puiSelectionBuffer_work;
	int iCountName;
	float fRange_near;
	float fRange_far;
	unsigned int uiNameArray[NAMEARRAYSIZE];
	float fRange_far_dest;

	puiSelectionBuffer_work = (GLuint*)uiSelectionBuffer;
	for(ui=0;ui<uiCountHit;ui++)
	{
		iCountName = (int)*puiSelectionBuffer_work;  //The number of the name of object which hit
		puiSelectionBuffer_work++;
		fRange_near = (float)*puiSelectionBuffer_work / 0x7fffffff;  //The region of objectwhich hit (front)
		puiSelectionBuffer_work++;
		fRange_far = (float) *puiSelectionBuffer_work / 0x7fffffff;

		//Get the name
		for(iIndexName = 0; iIndexName < iCountName; iIndexName++)
		{
			puiSelectionBuffer_work++;
			uiNameArray[iIndexName] =*puiSelectionBuffer_work;
		}
		puiSelectionBuffer_work++;

		if(NAMEARRAYSIZE < iCountName)
		{
			ASSERT(!"COpenGLPickView::SelectionBuffer2NameArray");
			continue;
		}

		//The element of most front is chosen. (Range of far is compareed)
		//In the case of the same range_far , priority is given in order of point, line, surface.
		if(0==ui)
		{
			for(iIndexName = 0; iIndexName < iCountName; iIndexName++)
			{
				uiNameArray_dest[iIndexName] = uiNameArray[iIndexName];
			}
			fRange_far_dest = fRange_far;
		}
		else
		{
			if(fRange_far < fRange_far_dest)		
			{
				for(iIndexName = 0; iIndexName < iCountName; iIndexName++)
				{
					uiNameArray_dest[iIndexName] = uiNameArray[iIndexName];
				}
				fRange_far_dest = fRange_far;
			}
			else if(fRange_far == fRange_far_dest && uiNameArray[1] < uiNameArray_dest[1])
			{
				for(iIndexName = 0; iIndexName < iCountName; iIndexName++)
				{
					uiNameArray_dest[iIndexName] = uiNameArray[iIndexName];
				}
				fRange_far_dest = fRange_far;
			}
		}
	}
}


void COpenGLPickView::OnRButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	if(0<DoPicking(point.x,point.y))
	{
		Invalidate(false);
		return;
	}
	COpenGLTrackView::OnRButtonUp(nFlags, point);

}
