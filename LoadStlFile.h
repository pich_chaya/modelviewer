// LoadStlFile.h
//
#pragma once

#include "ModelDataStructure.h"

bool LoadStlFile(const char* pszFileName, CModel& model );
bool ReadAsciiStl(const char* pszFileName, CModel& model );
bool ReadBinaryStl(const char* pszFileName, CModel& model );
void ConstructModel(int uiIndexVertex,int uiIndexTriangle,std::vector<vec>& tempVertices,CModel& model);