
// OpenGLBaseView.h : interface of the COpenGLBaseView class
//

#pragma once

#include <gl\gl.h> 
#include <gl\glu.h>

class COpenGLBaseView : public CView
{
		//member variable
	private:
	HGLRC	m_hRC;
	CDC*	m_pDC;
	GLuint	m_uiDisplayListIndex_StockScene;
protected:
	int		m_cx;
	int		m_cy;
	//member function
private:
	bool	InitializeOpenGL();
	void	UninitializeOpenGL();
	bool	SetupPixelFormat();
	bool	SetupViewport();
	bool	PreRenderScene(){return true;}
	void	RenderStockScene();
	bool	PostRenderScene(){return true;}
	void	RenderAxis();
	
protected:
	virtual bool	RenderScene(){return true;}
	virtual bool	SetupViewingFrustum();
	virtual bool	SetupViewingTransform();
protected: // create from serialization only
	COpenGLBaseView();
	DECLARE_DYNCREATE(COpenGLBaseView)

// Attributes
public:
	CDocument* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:

// Implementation
public:
	virtual ~COpenGLBaseView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	afx_msg void OnFilePrintPreview();
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnDestroy();
};

#ifndef _DEBUG  // debug version in OpenGLBaseView.cpp
inline CDocument* COpenGLBaseView::GetDocument() const
   { return reinterpret_cast<CDocument*>(m_pDocument); }
#endif

