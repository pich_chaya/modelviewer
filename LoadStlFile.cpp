//
// LoadStlFile.cpp
//
#include "stdafx.h"
#include <fstream>
#include <string>
#include "ModelDataStructure.h"
#include "io.h"
#include "Vec.h"
#include <map>

using namespace std;

#define	READSTRINGBUFFERLEN	256
void ConstructModel(int uiIndexVertex,int uiIndexTriangle,std::vector<vec>& tempVertices,CModel& model)
{
	map<vec,int> v;
	int cnt = 0;
	int *faces = new int[uiIndexVertex];
	for(unsigned int i = 0; i<uiIndexVertex; i++)
	 {
		map<vec,int>::iterator it;
		it = v.find(tempVertices[i]);

		if(it == v.end())
		{
			faces[i] = cnt;
			v.insert(make_pair(tempVertices[i],cnt++));
		}
		else
		{
			faces[i] = (*it).second;
		}
	 }
	 //Resize model Vertices & Triangle
	 int numVertex = cnt;
	 model.vVertex.resize(cnt);
	 model.vIndexedTriangle.resize(uiIndexTriangle);
	
	 //Move temp to the real model
	 map<vec, int>::iterator it = v.begin();
	 while(it != v.end())
	 {
		 model.vVertex[(*it).second] = (*it).first;
		 ++it;
	 }

	 for(unsigned int i = 0; i<uiIndexTriangle;i++)
	 {
		 model.vIndexedTriangle[i][0] = faces[i*3+0];
		 model.vIndexedTriangle[i][1] = faces[i*3+1];
	     model.vIndexedTriangle[i][2] = faces[i*3+2];

		 model.connectivity[model.vIndexedTriangle[i][0]].push_back(i); 
		 model.connectivity[model.vIndexedTriangle[i][1]].push_back(i); 
		 model.connectivity[model.vIndexedTriangle[i][2]].push_back(i); 
	 }
	
}

//Read Ascii Stl
bool ReadAsciiStl(const char* pszFileName, CModel& model)
{
	if( NULL == pszFileName )
	{
		return false;
	}

	FILE* pFILE;
	if( 0 != fopen_s( &pFILE, pszFileName, "rt" ) ) 
	{
		return false;
	}
	// Calculate the number of triangles
	unsigned int uiCountTriangle = 0;
	while( 1 )
	{
		char	szReadString[READSTRINGBUFFERLEN];
		if( NULL == fgets( szReadString, READSTRINGBUFFERLEN, pFILE ) )
		{
			break;
		}
		const char 	cszDelimiter[] = ", \r\n\t";
		char* pszContext;
		char* pszToken = strtok_s( szReadString, cszDelimiter, &pszContext );
		if( NULL == pszToken )
		{
			continue;
		}
		if( 0 == _stricmp( pszToken, "facet" ) )
		{
			uiCountTriangle++;
			continue;
		}
	}
	rewind( pFILE );

	if( 0 == uiCountTriangle )
	{
		fclose( pFILE );
		return false;
	}

	model.connectivity.resize(uiCountTriangle*3);
	model.vFacesNormal.resize(uiCountTriangle);
	model.vVertexNormal.resize(uiCountTriangle * 3);

	unsigned int ui3 = 0;
	unsigned int uiIndexTriangle = 0;
	unsigned int uiIndexVertex = 0;
	
	
	std::vector<vec> tempVertices;
	tempVertices.resize(uiCountTriangle*3);

	while( 1 )
	{
		char szReadString[READSTRINGBUFFERLEN];
		

		if( NULL == fgets( szReadString, READSTRINGBUFFERLEN, pFILE ) )
		{
			break;
		}
		const char 	cszDelimiter[] = ", \r\n\t";
		char* pszContext;
		char* pszToken = strtok_s( szReadString, cszDelimiter, &pszContext );
		if( NULL == pszToken )
		{
			continue;
		}

		if( 0 == _stricmp( pszToken, "vertex" ) )
		{
			if( 3 <= ui3 )
			{
				continue;
			}

			pszToken = strtok_s( NULL, cszDelimiter, &pszContext );
			tempVertices[uiIndexVertex][0] = (float)atof( pszToken );
			
			pszToken = strtok_s( NULL, cszDelimiter, &pszContext );
			tempVertices[uiIndexVertex][1] = (float)atof( pszToken );

			pszToken = strtok_s( NULL, cszDelimiter, &pszContext );
			tempVertices[uiIndexVertex][2] = (float)atof( pszToken );
		

			uiIndexVertex++;
			ui3++;

			continue;
		}
		else if( 0 == _stricmp( pszToken, "facet" ) )
		{	// normal vector of surface
			ui3 = 0;
			continue;
		}
		else if( 0 == _stricmp( pszToken, "endfacet" ) )
		{
			uiIndexTriangle++;
			continue;
		}
		else if( 0 == _stricmp( pszToken, "solid" ) )
		{	// solid name
			continue;
		}
	}

	fclose( pFILE );

	if( 0 == tempVertices.size())
	{
		return false;
	}

	ConstructModel(uiIndexVertex,uiIndexTriangle,tempVertices,model);

	return true;
}

//Read Binary Stl
bool ReadBinaryStl( const char* pszFileName, CModel& model )
{

	unsigned int uiIndexTriangle = 0;
	unsigned int uiIndexVertex = 0;

	std::ifstream fin(pszFileName,ios::in | ios::binary);
	if(!fin)
	{
		AfxMessageBox( _T("Failed to open files") );
		return false;
	}

	fin.seekg(80,ios_base::beg);
	fin.read((char*)&uiIndexTriangle,sizeof(unsigned int));

	uiIndexVertex = uiIndexTriangle*3;

	
	model.vVertexNormal.resize(uiIndexVertex );
	model.vFacesNormal.resize(uiIndexTriangle);
	model.connectivity.resize(uiIndexVertex);

	//temp vertices
	std::vector<vec> tempVertices;
	tempVertices.resize(uiIndexVertex);

	//Read file and put data in tempVertices
	float x1,y1,z1,x2,y2,z2,x3,y3,z3;
	for(unsigned int i=0; i<uiIndexTriangle;i++)
	{
		fin.seekg(12,ios_base::cur);
		fin.read((char*)&x1,sizeof(float));
		fin.read((char*)&y1,sizeof(float));
		fin.read((char*)&z1,sizeof(float));
		
		fin.read((char*)&x2,sizeof(float));
		fin.read((char*)&y2,sizeof(float));
		fin.read((char*)&z2,sizeof(float));

		fin.read((char*)&x3,sizeof(float));
		fin.read((char*)&y3,sizeof(float));
		fin.read((char*)&z3,sizeof(float));

		tempVertices[i*3+0][0] = x1;
		tempVertices[i*3+0][1] = y1;
		tempVertices[i*3+0][2] = z1;

		tempVertices[i*3+1][0] = x2;
		tempVertices[i*3+1][1] = y2;
		tempVertices[i*3+1][2] = z2;

		tempVertices[i*3+2][0] = x3;
		tempVertices[i*3+2][1] = y3;
		tempVertices[i*3+2][2] = z3;

		fin.seekg(2,ios_base::cur);
	}

	if( 0 == tempVertices.size() )
	{
		return false;
	}
	 fin.close();
	 
	 ConstructModel(uiIndexVertex,uiIndexTriangle,tempVertices,model);

	return true;
}

//Check BinaryStl or AsciiStl
bool LoadStlFile( const char* pszFileName, CModel& model )
{
	if( NULL == pszFileName )
	{
		return false;
	}

	FILE* pFILE;
	if( 0 != fopen_s( &pFILE, pszFileName, "rt" ) ) 
	{
		return false;
	}

	std::ifstream ifs(pszFileName);
	std::string buf;

	while(ifs >> buf){
		if(buf =="solid"){
			ReadAsciiStl(pszFileName, model);
			break;
		}
		else if(buf =="OFF" || buf == "ply"){
			return false;
		}
		else{
			ReadBinaryStl(pszFileName, model);
			break;
		}
	}
	return true;

}


