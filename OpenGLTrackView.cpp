
// OpenGLTrackView.cpp : implementation of the COpenGLTrackView class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
//#include "OpenGLTrack.h"
#endif

//#include "OpenGLTrackDoc.h"
#include "OpenGLTrackView.h"
#include "Math.h"

#ifndef M_PI
#define M_PI  3.1415926535897932384626433832795
#endif


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// COpenGLTrackView

IMPLEMENT_DYNCREATE(COpenGLTrackView, COpenGLBaseView)

BEGIN_MESSAGE_MAP(COpenGLTrackView, COpenGLBaseView)
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MBUTTONDOWN()
	ON_WM_MBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()

// COpenGLTrackView construction/destruction

COpenGLTrackView::COpenGLTrackView():
	m_eTrackingMode(TM_NONE),
	m_fRenderingRate(10)
{
	// TODO: add construction code here
	m_f3RenderingCenter[0]	=	0.0;
	m_f3RenderingCenter[1]	=	0.0;
	m_f3RenderingCenter[2]	=	0.0;

	m_objectXform[0][0] = 1.0f;
	m_objectXform[0][1] = 0.0f;
	m_objectXform[0][2] = 0.0f;
	m_objectXform[0][3] = 0.0f;

	m_objectXform[1][0] = 0.0f;
	m_objectXform[1][1] = 1.0f;
	m_objectXform[1][2] = 0.0f;
	m_objectXform[1][3] = 0.0f;

	m_objectXform[2][0] = 0.0f;
	m_objectXform[2][1] = 0.0f;
	m_objectXform[2][2] = 1.0f;
	m_objectXform[2][3] = 0.0f;

	m_objectXform[3][0] = 0.0f;
	m_objectXform[3][1] = 0.0f;
	m_objectXform[3][2] = 0.0f;
	m_objectXform[3][3] = 1.0f;

}
COpenGLTrackView::~COpenGLTrackView()
{
}

BOOL COpenGLTrackView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs
	cs.style |=WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
	return COpenGLBaseView::PreCreateWindow(cs);
}

// COpenGLTrackView drawing

//void COpenGLTrackView::OnDraw(CDC* /*pDC*/)
//{
//	CDocument* pDoc = GetDocument();
//	ASSERT_VALID(pDoc);
//	if (!pDoc)
//		return;
//
//	// TODO: add draw code for native data here
//}

void COpenGLTrackView::OnRButtonUp(UINT /* nFlags */, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void COpenGLTrackView::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
//	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}


// COpenGLTrackView diagnostics

#ifdef _DEBUG
void COpenGLTrackView::AssertValid() const
{
	COpenGLBaseView::AssertValid();
}

void COpenGLTrackView::Dump(CDumpContext& dc) const
{
	COpenGLBaseView::Dump(dc);
}

CDocument* COpenGLTrackView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CDocument)));
	return (CDocument*)m_pDocument;
}
#endif //_DEBUG


// COpenGLTrackView message handlers

void COpenGLTrackView::BeginTracking(CPoint point, ETRACKINGMODE eTrackingMode)
{
	SetCapture();
	m_eTrackingMode = eTrackingMode;
	switch(m_eTrackingMode)
	{
	case TM_NONE:
		break;
	case TM_SPIN:
		GLfloat p[2];
		p[0] = (GLfloat)point.x;
		p[1] = (GLfloat)point.y;
		ptov(p,m_f3LastPos,(GLfloat)m_cx,(GLfloat)m_cy);
		break;
	case TM_PAN:
		m_ptLast = point;
		break;
	case TM_ZOOM:
		m_ptLast = point;
		break;
	default:
		ASSERT(!"Uncorresponded");
		break;
	}


}

void COpenGLTrackView::ptov(GLfloat p[2], GLfloat v[3], GLfloat fWidth, GLfloat fHeight)
{
	GLfloat d,a;
	v[0] = (2.0f*p[0] - fWidth) / fWidth;
	v[1] = (fHeight - 2.0f*p[1]) / fHeight;
	v[0] = (2.0f*p[0] - fWidth) / fWidth*0.5f;
	v[1] = (fHeight - 2.0f*p[1]) / fHeight*0.5f;
	d = (GLfloat)sqrt(v[0]*v[0]+v[1]*v[1]);
	v[2] = (GLfloat)cos((M_PI/2.0f)*((d<1.0f)?d:1.0f));

	a = 1.0f/(GLfloat)sqrt(v[0]*v[0]+v[1]*v[1]+v[2]*v[2]);
	v[0] *= a;
	v[1] *= a;
	v[2] *= a;

}

void COpenGLTrackView::EndTracking()
{
	ReleaseCapture();
	m_eTrackingMode = TM_NONE;
}

void COpenGLTrackView::DoTracking(CPoint point)
{
	switch(m_eTrackingMode)
	{
		case TM_NONE:
			break;
		case TM_SPIN:
			{
				GLfloat p[2];
				p[0] = (GLfloat) point.x;
				p[1] = (GLfloat) point.y;
				GLfloat f3CurPos[3];
				ptov(p,f3CurPos, (GLfloat)m_cx,(GLfloat)m_cy);
				GLfloat dx = f3CurPos[0] - m_f3LastPos[0];
				GLfloat dy = f3CurPos[1] - m_f3LastPos[1];
				GLfloat dz = f3CurPos[2] - m_f3LastPos[2];
			
				float fAngle = 180.0f*(GLfloat)sqrt(dx*dx+dy*dy+dz*dz);
				float fX = m_f3LastPos[1]*f3CurPos[2] - m_f3LastPos[2]*f3CurPos[1];
				float fY = m_f3LastPos[2]*f3CurPos[0] - m_f3LastPos[0]*f3CurPos[2];
				float fZ = m_f3LastPos[0]*f3CurPos[1] - m_f3LastPos[1]*f3CurPos[0];
			
				m_f3LastPos[0] = f3CurPos[0];
				m_f3LastPos[1] = f3CurPos[1];
				m_f3LastPos[2] = f3CurPos[2];
				DoRotation(fAngle,fX,fY,fZ);
				Invalidate(FALSE);
			}


			break;
		case TM_PAN:
			{
				m_f3RenderingCenter[0] -= (point.x - m_ptLast.x) / m_fRenderingRate;
				m_f3RenderingCenter[1] += (point.y - m_ptLast.y) / m_fRenderingRate;
				m_ptLast = point;
				//modeling & viewing transformation
				::glMatrixMode(GL_MODELVIEW);
				::glLoadIdentity();
				SetupViewingTransform();
				Invalidate(FALSE);
			}
			break;
		
		case TM_ZOOM:
			{
				//scaleing
				//+500:2.0 magnificantion, +250: 1.5 magnificantion
				//-500:0.5 magnificantion, -1000:0.25magnificantion

				m_fRenderingRate *= (float)pow(2,(m_ptLast.y - point.y) * 0.002f);
				m_ptLast = point;

				//Setup view point frustum
				::glMatrixMode(GL_PROJECTION);
				::glLoadIdentity();
				SetupViewingFrustum();
				::glMatrixMode(GL_MODELVIEW);
				Invalidate(FALSE);
			}
			break;
			
		default:
			ASSERT(!"Uncorresponded");
			break;
	}

}

void COpenGLTrackView::DoRotation(float fAngle, float fX, float fY, float fZ)
{
	::glPushMatrix();
		::glLoadIdentity();
		::glRotatef(fAngle,fX,fY,fZ);
		::glMultMatrixf((GLfloat*)m_objectXform);
		::glGetFloatv(GL_MODELVIEW_MATRIX,(GLfloat*)m_objectXform);
	::glPopMatrix();

		//modeling&viewing transformation
		::glMatrixMode(GL_MODELVIEW);
		::glLoadIdentity();
		SetupViewingTransform();
}

bool COpenGLTrackView::SetupViewingFrustum()
{
	::glOrtho(-m_cx * 0.5 / m_fRenderingRate, //left
				m_cx * 0.5 / m_fRenderingRate, //right
				-m_cy*0.5 / m_fRenderingRate, //button
				m_cy*0.5 / m_fRenderingRate, //top
				10, //near
				10000.0);  //far

	return true;
}

bool COpenGLTrackView::SetupViewingTransform()
{
	::gluLookAt(m_f3RenderingCenter[0], m_f3RenderingCenter[1], 
				m_f3RenderingCenter[2]+500.0, //viewpoint
				m_f3RenderingCenter[0],
				m_f3RenderingCenter[1], 
				m_f3RenderingCenter[2], //gaze point
				0.0,1.0,0.0); //up vector
	::glMultMatrixf((GLfloat*)m_objectXform);
	return true;
}



void COpenGLTrackView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	BeginTracking(point, TM_SPIN);
	COpenGLBaseView::OnLButtonDown(nFlags, point);
}



void COpenGLTrackView::OnLButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	EndTracking();
	COpenGLBaseView::OnLButtonUp(nFlags, point);
}


void COpenGLTrackView::OnMButtonDown(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	BeginTracking(point, TM_PAN);
	COpenGLBaseView::OnMButtonDown(nFlags, point);
}


void COpenGLTrackView::OnMButtonUp(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	EndTracking();
	COpenGLBaseView::OnMButtonUp(nFlags, point);
}


void COpenGLTrackView::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: Add your message handler code here and/or call default
	DoTracking(point);
	COpenGLBaseView::OnMouseMove(nFlags, point);
}


BOOL COpenGLTrackView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	// TODO: Add your message handler code here and/or call default
	BeginTracking(pt,TM_ZOOM);
	CPoint pt_delta(pt.x - zDelta/3,pt.y - zDelta /3);
	DoTracking(pt_delta);
	EndTracking();
	return COpenGLBaseView::OnMouseWheel(nFlags, zDelta, pt);
}

