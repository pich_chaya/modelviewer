
// ModelViewerView.cpp : implementation of the CModelViewerView class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
#include "ModelViewer.h"
#endif

#include "ModelViewerDoc.h"
#include "ModelViewerView.h"
#include "LoadStlFile.h"
#include "LoadPlyFile.h"
#include "LoadOffFile.h"
#include "Calculation.h"
#include "Vec.h"
#include "color.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CModelViewerView

IMPLEMENT_DYNCREATE(CModelViewerView, COpenGLPickView)

	BEGIN_MESSAGE_MAP(CModelViewerView, COpenGLPickView)
		ON_WM_CONTEXTMENU()
		ON_WM_RBUTTONUP()
		ON_COMMAND(ID_VIEW_SPIN, &CModelViewerView::OnViewSpin)
		ON_UPDATE_COMMAND_UI(ID_VIEW_SPIN, &CModelViewerView::OnUpdateViewSpin)
		ON_COMMAND(ID_VIEW_PAN, &CModelViewerView::OnViewPan)
		ON_UPDATE_COMMAND_UI(ID_VIEW_PAN, &CModelViewerView::OnUpdateViewPan)
		ON_COMMAND(ID_VIEW_ZOOM, &CModelViewerView::OnViewZoom)
		ON_UPDATE_COMMAND_UI(ID_VIEW_ZOOM, &CModelViewerView::OnUpdateViewZoom)
		ON_COMMAND(ID_VIEW_RENDERPOINT, &CModelViewerView::OnViewRenderpoint)
		ON_UPDATE_COMMAND_UI(ID_VIEW_RENDERPOINT, &CModelViewerView::OnUpdateViewRenderpoint)
		ON_COMMAND(ID_VIEW_RENDERLINE, &CModelViewerView::OnViewRenderline)
		ON_UPDATE_COMMAND_UI(ID_VIEW_RENDERLINE, &CModelViewerView::OnUpdateViewRenderline)
		ON_COMMAND(ID_VIEW_RENDERFACE, &CModelViewerView::OnViewRenderface)
		ON_UPDATE_COMMAND_UI(ID_VIEW_RENDERFACE, &CModelViewerView::OnUpdateViewRenderface)
		ON_COMMAND(ID_VIEW_PICKPOINT, &CModelViewerView::OnViewPickpoint)
		ON_UPDATE_COMMAND_UI(ID_VIEW_PICKPOINT, &CModelViewerView::OnUpdateViewPickpoint)
		ON_COMMAND(ID_VIEW_PICKLINE, &CModelViewerView::OnViewPickline)
		ON_UPDATE_COMMAND_UI(ID_VIEW_PICKLINE, &CModelViewerView::OnUpdateViewPickline)
		ON_COMMAND(ID_VIEW_PICKFACE, &CModelViewerView::OnViewPickface)
		ON_UPDATE_COMMAND_UI(ID_VIEW_PICKFACE, &CModelViewerView::OnUpdateViewPickface)
		ON_COMMAND(ID_FILE_OPEN, &CModelViewerView::OnFileOpen)
		ON_COMMAND(ID_FILE_NEW, &CModelViewerView::OnFileNew)
		ON_COMMAND(ID_DRAWING_NORMALVERTICES, &CModelViewerView::OnDrawingNormalvertices)
		ON_UPDATE_COMMAND_UI(ID_DRAWING_NORMALVERTICES, &CModelViewerView::OnUpdateDrawingNormalvertices)
		ON_COMMAND(ID_DRAWING_FACENORMAL, &CModelViewerView::OnDrawingFacenormal)
		ON_UPDATE_COMMAND_UI(ID_DRAWING_FACENORMAL, &CModelViewerView::OnUpdateDrawingFacenormal)
		ON_COMMAND(ID_DRAWING_CURVATURE, &CModelViewerView::OnDrawingCurvature)
		ON_UPDATE_COMMAND_UI(ID_DRAWING_CURVATURE, &CModelViewerView::OnUpdateDrawingCurvature)
	END_MESSAGE_MAP()

	// CModelViewerView construction/destruction

	CModelViewerView::CModelViewerView():
		m_bSpin(TRUE),
			m_bPan(TRUE),
			m_bZoom(TRUE),
			m_bRenderPoint(TRUE),
			m_bRenderLine(TRUE),
			m_bRenderFace(TRUE),
			m_bPickPoint(TRUE),
			m_bPickLine(TRUE),
			m_bPickFace(TRUE),
			m_VertexNormal(FALSE),
			m_FaceNormal(FALSE),
			m_curvature(FALSE),
			m_uiDisplayListIndex_Point(0),
			m_uiDisplayListIndex_Line(0),
			m_uiDisplayListIndex_NormalLine(0),
			m_uiDisplayListIndex_FaceNormalLine(0),
			m_uiDisplayListIndex_Face(0),
			m_uiDisplayListIndex_curvature(0)
			
		{
			
		}

		CModelViewerView::~CModelViewerView()
		{
		}

		BOOL CModelViewerView::PreCreateWindow(CREATESTRUCT& cs)
		{
			// TODO: Modify the Window class or styles here by modifying
			//  the CREATESTRUCT cs

			return COpenGLPickView::PreCreateWindow(cs);
		}


		void CModelViewerView::OnRButtonUp(UINT  nFlags, CPoint point)
		{
			if(0 < DoPicking(point.x,point.y))
			{
				Invalidate(false);
				return;
			}

			COpenGLTrackView::OnRButtonUp(nFlags, point);
		}


		void CModelViewerView::OnContextMenu(CWnd* /* pWnd */, CPoint point)
		{
#ifndef SHARED_HANDLERS
			theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
		}


		// CModelViewerView diagnostics

#ifdef _DEBUG
		void CModelViewerView::AssertValid() const
		{
			COpenGLPickView::AssertValid();
		}

		void CModelViewerView::Dump(CDumpContext& dc) const
		{
			COpenGLPickView::Dump(dc);
		}

		CModelViewerDoc* CModelViewerView::GetDocument() const // non-debug version is inline
		{
			ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CModelViewerDoc)));
			return (CModelViewerDoc*)m_pDocument;
		}
#endif //_DEBUG


		// CModelViewerView message handlers

		void CModelViewerView::OnViewSpin()
		{
			m_bSpin = !m_bSpin;
		}


		void CModelViewerView::OnUpdateViewSpin(CCmdUI *pCmdUI)
		{
			pCmdUI->SetCheck(m_bSpin);
		}


		void CModelViewerView::OnViewPan()
		{
			m_bPan = !m_bPan;
		}


		void CModelViewerView::OnUpdateViewPan(CCmdUI *pCmdUI)
		{
			pCmdUI->SetCheck(m_bPan);
		}


		void CModelViewerView::OnViewZoom()
		{
			m_bZoom = !m_bZoom;
		}


		void CModelViewerView::OnUpdateViewZoom(CCmdUI *pCmdUI)
		{
			pCmdUI->SetCheck(m_bZoom);
		}


		void CModelViewerView::OnViewRenderpoint()
		{
			m_bRenderPoint = !m_bRenderPoint;
			Invalidate(FALSE);
		}


		void CModelViewerView::OnUpdateViewRenderpoint(CCmdUI *pCmdUI)
		{
			pCmdUI->SetCheck(m_bRenderPoint);
		}


		void CModelViewerView::OnViewRenderline()
		{
			m_bRenderLine = !m_bRenderLine;
			Invalidate(FALSE);
		}


		void CModelViewerView::OnUpdateViewRenderline(CCmdUI *pCmdUI)
		{
			pCmdUI->SetCheck(m_bRenderLine);
		}


		void CModelViewerView::OnViewRenderface()
		{
			m_bRenderFace = !m_bRenderFace;
			Invalidate(FALSE);
		}


		void CModelViewerView::OnUpdateViewRenderface(CCmdUI *pCmdUI)
		{
			pCmdUI->SetCheck(m_bRenderFace);
		}


		void CModelViewerView::OnViewPickpoint()
		{
			m_bPickPoint = !m_bPickPoint;
			Invalidate(FALSE);
		}


		void CModelViewerView::OnUpdateViewPickpoint(CCmdUI *pCmdUI)
		{
			pCmdUI->SetCheck(m_bPickPoint);
		}


		void CModelViewerView::OnViewPickline()
		{
			m_bPickLine = !m_bPickLine;
			Invalidate(FALSE);
		}


		void CModelViewerView::OnUpdateViewPickline(CCmdUI *pCmdUI)
		{
			pCmdUI->SetCheck(m_bPickLine);
		}


		void CModelViewerView::OnViewPickface()
		{
			m_bPickFace = !m_bPickFace;
			Invalidate(FALSE);
		}


		void CModelViewerView::OnUpdateViewPickface(CCmdUI *pCmdUI)
		{
			pCmdUI->SetCheck(m_bPickFace);
		}

		//Normal Vertex
		void CModelViewerView::OnDrawingNormalvertices()
		{
			m_VertexNormal = !m_VertexNormal;
			Invalidate(FALSE);

		}
		void CModelViewerView::OnUpdateDrawingNormalvertices(CCmdUI *pCmdUI)
		{
			pCmdUI->SetCheck(m_VertexNormal);
		}

		//Face Vertex		
		void CModelViewerView::OnDrawingFacenormal()
		{
			m_FaceNormal = !m_FaceNormal;
				Invalidate(FALSE);
		}


		void CModelViewerView::OnUpdateDrawingFacenormal(CCmdUI *pCmdUI)
		{
			pCmdUI->SetCheck(m_FaceNormal);
		}



		void CModelViewerView::OnDrawingCurvature()
		{
			// TODO: Add your command handler code here
			m_curvature = !m_curvature;
			Invalidate(FALSE);
		}


		void CModelViewerView::OnUpdateDrawingCurvature(CCmdUI *pCmdUI)
		{
			// TODO: Add your command update UI handler code here
			pCmdUI->SetCheck(m_curvature);
		}


		void CModelViewerView::DoTracking(CPoint point)
		{
			if((TM_SPIN == m_eTrackingMode && !m_bSpin) 
				|| (TM_PAN == m_eTrackingMode && !m_bPan) 
				|| (TM_ZOOM == m_eTrackingMode && !m_bZoom))
			{
				return;
			}

			COpenGLPickView::DoTracking(point);
		}



		void CModelViewerView::RenderObjects(ERENDERMODE eRenderMode, const unsigned int*auiName)
		{	
			unsigned int uiIndexTriangle;
			unsigned int uiIndexPoint;
			unsigned int ui3; 

			switch(eRenderMode)
			{
			case RM_RENDER:
			case RM_PICK:
				::glPushAttrib(GL_ALL_ATTRIB_BITS);
				{
					if(m_bRenderFace && !(RM_PICK == eRenderMode && !m_bPickFace))
					{
						//surface
						if(RM_RENDER == eRenderMode)
						{
							//drawing

							if(0==m_uiDisplayListIndex_Face)
							{
								m_uiDisplayListIndex_Face = ::glGenLists(1);
								::glNewList(m_uiDisplayListIndex_Face,GL_COMPILE);
								//::glColor3f(0.5,0.5,0);
								glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);

								//Enable Lighting
								glEnable(GL_LIGHTING);
								glEnable(GL_LIGHT0);
								glEnable(GL_NORMALIZE);
								

								//////Set light intensity and color
								GLfloat light_ambient[] = { 0.2, 0.2, 0.2, 1.0 };
								GLfloat light_diffuse[] = { 1.0, 1.0, 1.0, 1.0};
								GLfloat light_specular[] = { 1.0, 1.0, 1.0, 1.0 };
		

								glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
								glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);

								GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
								glShadeModel (GL_SMOOTH);

								glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE, mat_specular);
								glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);

								::glBegin(GL_TRIANGLES);

								vec rgb;
								for(uiIndexTriangle = 0; uiIndexTriangle<m_model.vIndexedTriangle.size();  uiIndexTriangle++)
								{	
										glNormal3fv( m_model.vVertexNormal[m_model.vIndexedTriangle[uiIndexTriangle][0]]);
										glVertex3fv( m_model.vVertex[ m_model.vIndexedTriangle[uiIndexTriangle][0]]);
									
										glNormal3fv( m_model.vVertexNormal[m_model.vIndexedTriangle[uiIndexTriangle][1]]);
										glVertex3fv( m_model.vVertex[ m_model.vIndexedTriangle[uiIndexTriangle][1]]);
									
										glNormal3fv( m_model.vVertexNormal[m_model.vIndexedTriangle[uiIndexTriangle][2]]);
										glVertex3fv( m_model.vVertex[ m_model.vIndexedTriangle[uiIndexTriangle][2]]);
									
									
								}
								::glEnd();
								
								glDisable(GL_LIGHTING);
								glDisable(GL_LIGHT0);
								
								::glEndList();
								
							}

							::glCallList(m_uiDisplayListIndex_Face);

							//Pick surface
							if(GL_FILL == m_auiName_pick[1])
							{
								uiIndexTriangle = m_auiName_pick[2];
								::glPushAttrib(GL_CURRENT_BIT);
								::glColor3f(1.0,1.0,0.0);
								glBegin(GL_TRIANGLES);
								glVertex3fv(m_model.vVertex[ m_model.vIndexedTriangle[uiIndexTriangle][0]]);
								glVertex3fv(m_model.vVertex[ m_model.vIndexedTriangle[uiIndexTriangle][1]]);
								glVertex3fv(m_model.vVertex[ m_model.vIndexedTriangle[uiIndexTriangle][2]]);
								::glEnd();
								::glPopAttrib(); //uiIndexTriangle
							}
						}

						else
						{ //pick drawing
							::glPushName(GL_FILL);
							::glColor3f(0.5,0.5,0);
							glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);
							for(uiIndexTriangle = 0; uiIndexTriangle<m_model.vIndexedTriangle.size(); uiIndexTriangle++)
							{
								::glPushName(uiIndexTriangle);
								::glBegin(GL_TRIANGLES);
								glVertex3fv(m_model.vVertex[ m_model.vIndexedTriangle[uiIndexTriangle][0]]);
								glVertex3fv(m_model.vVertex[ m_model.vIndexedTriangle[uiIndexTriangle][1]]);
								glVertex3fv(m_model.vVertex[ m_model.vIndexedTriangle[uiIndexTriangle][2]]);
								glEnd();
								glPopName();  //uiIndexTriangle

							}

							glPopName(); //GL_FILL

						}

					}
					
					if(m_VertexNormal)
					{
						//normal vector
						if(RM_RENDER == eRenderMode)
						{
							//drawing
							if(0==m_uiDisplayListIndex_NormalLine)
							{
								m_uiDisplayListIndex_NormalLine = ::glGenLists(1);
								::glNewList(m_uiDisplayListIndex_NormalLine,GL_COMPILE);
								::glColor3f(0.5,0.5,1);
								::glLineWidth(3.0);
								glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
								
								::glBegin(GL_LINES);

								for(uiIndexTriangle = 0; uiIndexTriangle<m_model.vVertex.size();  uiIndexTriangle++)
								{
									glVertex3fv(m_model.vVertex[uiIndexTriangle]);
									glVertex3fv(m_model.vVertex[uiIndexTriangle] + m_model.vVertexNormal[uiIndexTriangle]);
								}
								
								glPopName();
								::glEnd();
								::glEndList();
								
							}

							::glCallList(m_uiDisplayListIndex_NormalLine);

						}
						else
						{	
							
						}
					}

					if(m_FaceNormal)
					{
						//normal vector
						if(RM_RENDER == eRenderMode)
						{
							//drawing
							if(0==m_uiDisplayListIndex_FaceNormalLine)
							{
								m_uiDisplayListIndex_FaceNormalLine = ::glGenLists(1);
								::glNewList(m_uiDisplayListIndex_FaceNormalLine,GL_COMPILE);
								::glColor3f(0.5,0.5,1);
								::glLineWidth(3.0);
								glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
								
								::glBegin(GL_LINES);
								for(int i=0;i<m_model.vIndexedTriangle.size();i++) {
									vec p;
									p+=m_model.vVertex[m_model.vIndexedTriangle[i][0]];
									p+=m_model.vVertex[m_model.vIndexedTriangle[i][1]];
									p+=m_model.vVertex[m_model.vIndexedTriangle[i][2]];
									p/=3.0f;
									glVertex3fv(p);
									glVertex3fv(p + m_model.vFacesNormal[i]);
								}

								glPopName();
								::glEnd();
								::glEndList();
								
							}

							::glCallList(m_uiDisplayListIndex_FaceNormalLine);

						}
						else
						{	
							
						}
					}
					
					if(m_curvature)
					{
						if(RM_RENDER == eRenderMode)
						{
							if(0==m_uiDisplayListIndex_curvature)
							{
								m_uiDisplayListIndex_curvature = ::glGenLists(1);
								::glNewList(m_uiDisplayListIndex_curvature,GL_COMPILE);
								glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);

								vec rgb;
								::glBegin(GL_TRIANGLES);
								for(uiIndexTriangle = 0; uiIndexTriangle<m_model.vIndexedTriangle.size();  uiIndexTriangle++)
								{
									rgb = HSVtoRGB(
										vec(m_model.curvature[m_model.vIndexedTriangle[uiIndexTriangle][0]]*0.7, 1.0f, 1.0f)
										);
									glColor3fv(rgb);
									glVertex3fv( m_model.vVertex[ m_model.vIndexedTriangle[uiIndexTriangle][0]]);


									rgb = HSVtoRGB(
										vec(m_model.curvature[m_model.vIndexedTriangle[uiIndexTriangle][1]]*0.7, 1.0f, 1.0f)
										);
									glColor3fv(rgb);
									glVertex3fv( m_model.vVertex[ m_model.vIndexedTriangle[uiIndexTriangle][1]]);


									rgb = HSVtoRGB(
										vec(m_model.curvature[m_model.vIndexedTriangle[uiIndexTriangle][2]]*0.7, 1.0f, 1.0f)
										);
									glColor3fv(rgb);
									glVertex3fv( m_model.vVertex[ m_model.vIndexedTriangle[uiIndexTriangle][2]]);

								}

								glPopName();
								::glEnd();
								::glEndList();
							}

							::glCallList(m_uiDisplayListIndex_curvature);
						}
					}

					if(m_bRenderLine && !(RM_PICK == eRenderMode && !m_bPickLine))
					{
						//line
						if(RM_RENDER == eRenderMode)
						{
							//drawing
							if(0 == m_uiDisplayListIndex_Line)
							{
								m_uiDisplayListIndex_Line = ::glGenLists(1);
								::glNewList(m_uiDisplayListIndex_Line,GL_COMPILE);
								::glLineWidth(1.5);
								::glColor3f(0.0,0.5,0.5);
								glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
								glBegin(GL_TRIANGLES);
								for(uiIndexTriangle = 0; uiIndexTriangle<m_model.vIndexedTriangle.size(); uiIndexTriangle++)
								{
									glVertex3fv( m_model.vVertex[ m_model.vIndexedTriangle[uiIndexTriangle][0]]);
									glVertex3fv( m_model.vVertex[ m_model.vIndexedTriangle[uiIndexTriangle][1]]);
									glVertex3fv( m_model.vVertex[ m_model.vIndexedTriangle[uiIndexTriangle][2]]);

								}
								::glEnd();
								::glEndList();
							}
							::glCallList(m_uiDisplayListIndex_Line);

							//picked Line
							if(GL_LINE == m_auiName_pick[1])
							{
								uiIndexTriangle = m_auiName_pick[2];
								ui3 = m_auiName_pick[3];
								::glPushAttrib(GL_CURRENT_BIT | GL_LINE_BIT);
								glLineWidth(5.0);
								::glColor3f(0.0,1.0,1.0);
								::glBegin(GL_LINES);
								//unsigned int uiIndexVertex0 = m_model.vIndexedTriangle[uiIndexTriangle].ui3IndexVertex[ui3];
								unsigned int uiIndexVertex0 = m_model.vIndexedTriangle[uiIndexTriangle][ui3];

								//unsigned int uiIndexVertex1 = m_model.vIndexedTriangle[uiIndexTriangle].ui3IndexVertex[(ui3+1)%3];

								unsigned int uiIndexVertex1 = m_model.vIndexedTriangle[uiIndexTriangle][(ui3+1)%3];
								glVertex3fv(m_model.vVertex[uiIndexVertex0]);
								glVertex3fv(m_model.vVertex[uiIndexVertex1]); 

								::glEnd();
								::glPopAttrib();
							}
						}
						else
						{	//pick drawing
							::glPushName(GL_LINE);
							::glLineWidth(2.0);
							::glColor3f(0.0,0.5,0.5);
							for(uiIndexTriangle = 0; uiIndexTriangle<m_model.vIndexedTriangle.size(); uiIndexTriangle++)
							{
								::glPushName(uiIndexTriangle);
								for(ui3 = 0;ui3<3 ;ui3++)
								{
									::glPushName(ui3);
									::glBegin(GL_LINES);
									unsigned int uiIndexVertex0 = m_model.vIndexedTriangle[uiIndexTriangle][ui3];
									//unsigned int uiIndexVertex1 = m_model.vIndexedTriangle[uiIndexTriangle].ui3IndexVertex[(ui3+1)%3];
									unsigned int uiIndexVertex1 = m_model.vIndexedTriangle[uiIndexTriangle][(ui3+1)%3];
									glVertex3fv(m_model.vVertex[uiIndexVertex0]);
									glVertex3fv(m_model.vVertex[uiIndexVertex1]); 

									::glEnd();
									::glPopName(); //::glPushName(ui3)

								}

								::glPopName();  //iIndexLine
							}
							::glPopName();  //GL_LINE

						}
					}


					if(m_bRenderPoint && !(RM_PICK == eRenderMode && !m_bPickPoint))
					{
						//point
						if(RM_RENDER == eRenderMode)
						{
							//drawing
							if(0 == m_uiDisplayListIndex_Point)
							{
								m_uiDisplayListIndex_Point = ::glGenLists(1);
								::glNewList(m_uiDisplayListIndex_Point,GL_COMPILE);
								::glPointSize(2.5);
								::glColor3f(0.5,0.0,0.5);
								::glBegin(GL_POINTS);
								for(uiIndexPoint = 0; uiIndexPoint < m_model.vVertex.size(); uiIndexPoint++)
								{
									glVertex3fv( m_model.vVertex[uiIndexPoint] );
								}
							
								::glEnd();

								::glEndList();
							}

							::glCallList(m_uiDisplayListIndex_Point);

							//picked point
							if(GL_POINT == m_auiName_pick[1])
							{
								uiIndexPoint = m_auiName_pick[2];
								::glPushAttrib(GL_CURRENT_BIT | GL_POINT_BIT);
								::glPointSize(10.0);
								::glColor3f(1.0,0.0,1.0);
								::glBegin(GL_POINTS);
								glVertex3fv(m_model.vVertex[uiIndexPoint] );
								::glEnd();
								::glPopAttrib(); //iIndexPoint
							}
						}
						else
						{	//pick drawing
							::glPushName(GL_POINT);
							::glPointSize(5.0);
							for(uiIndexPoint = 0; uiIndexPoint < m_model.vVertex.size(); uiIndexPoint++)
							{
								::glPushName(uiIndexPoint);
								::glBegin(GL_POINTS);
								glVertex3fv(m_model.vVertex[uiIndexPoint] );
								::glEnd();
								::glPopName(); //uiIndexPoint
							}

							::glPopName(); //GL_POINT

						}
					}
				}


				::glPopAttrib();
				return;

			case RM_SELECT:
				{
					if(NULL == auiName)
					{
						ASSERT(!"COpenGLPickView::RenderObjects");
						return;
					}

					for(int iIndexName = 0; iIndexName < NAMEARRAYSIZE; iIndexName++)
					{
						m_auiName_pick[iIndexName] = auiName[iIndexName];
					}
					switch(auiName[1])
					{
						//Processing according to "the picked object"
					case GL_POINT:
					case GL_LINE:
					case GL_FILL:
						return;

					default:
						ASSERT(!"COpenGLPickView::RenderObjects");
						return;
					}
				}
				return;

			default:
				ASSERT(!"COpenGLPickView::RenderObjects");
				return;

			}

		}
		


		void CModelViewerView::OnFileOpen()
		{
			// TODO: Add your command handler code here
			CFileDialog		dlg( TRUE );
			dlg.m_ofn.lpstrFilter	=	_T("STL file (*.stl)\0*.stl\0")
				_T("PLY file (*.ply)\0*.ply\0")
				_T("OFF file (*.off)\0*.off\0")
				_T("All Files (*.*)\0*.*\0\0");
			dlg.m_ofn.nFilterIndex	= 1;
			dlg.m_ofn.Flags		|= OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;

			CString sFilePath;

			INT_PTR iResponse = dlg.DoModal();
			if( iResponse == IDOK )
			{
				char* pszFileName;

#ifdef UNICODE
				size_t length = wcslen( dlg.GetPathName().GetBuffer() ) + 1;
				pszFileName = (LPSTR)malloc( length * sizeof(WCHAR) );
				WideCharToMultiByte(	CP_ACP, // CODE PAGE: ANSI code page
					0,
					dlg.GetPathName().GetBuffer(),
					-1,
					pszFileName,
					(int)length * sizeof(WCHAR),
					NULL,
					NULL);
#else
				pszFileName = _strdup( dlg.GetPathName().GetBuffer() );
#endif
				CModel model;

				sFilePath = CString(pszFileName,length);
				if(LoadStlFile( pszFileName, model ) )
				{
					memset( m_auiName_pick, 0x00, NAMEARRAYSIZE * sizeof(unsigned int) );
					m_model = model;
					DeleteDisplayList();
					
				}

				else if(LoadPlyFile(pszFileName, model ) )
				{
					memset( m_auiName_pick, 0x00, NAMEARRAYSIZE * sizeof(unsigned int) );
					m_model = model;
					DeleteDisplayList();
				}

				else if(LoadOffFile(pszFileName, model ) )
				{
					memset( m_auiName_pick, 0x00, NAMEARRAYSIZE * sizeof(unsigned int) );
					m_model = model;
					DeleteDisplayList();
				}

				else
				{
					AfxMessageBox( _T("Failed to load files") );
				}


				free( pszFileName );
				Invalidate();

			}
			

			//calculate center of gravity, volume and area
			CalcCenterOfGravity(m_model);
			CalcArea(m_model);
			CalcCenter(m_model);
		
			

			//Normal surface
			m_model.FaceNormal();
			
			//Normal Vector
			m_model.VertexNormal();

			//curvature
			CalcCurvature(m_model);

			for(int i=0;i<m_model.vVertex.size();i++)
			{
				m_model.vVertex[i] = m_model.vVertex[i]- m_model.center;
			}


			CString sFileName = dlg.GetFileName();
			CString s,sVolume,sArea,sCentre;

			s.Format(_T("vertex : %d"),m_model.vVertex.size());
			sArea.Format(_T("Area : %.2f"),m_model.totalArea);
			sVolume.Format(_T("Volume : %.2f"),m_model.volume);
			sCentre.Format(_T("Centre : %2f, %2f, %2f"),m_model.centerOfGravity[0],m_model.centerOfGravity[1],m_model.centerOfGravity[2]);

			CFrameWnd* pFrame = (CFrameWnd*)AfxGetApp()->m_pMainWnd;
			CMFCStatusBar* pStatusBar = (CMFCStatusBar*)pFrame->GetDescendantWindow(AFX_IDW_STATUS_BAR);

			pStatusBar->SetPaneWidth(0,100);
			pStatusBar->SetPaneText(1,sFilePath);
			pStatusBar->SetPaneWidth(1,sFilePath.GetLength()*4);
			
			pStatusBar->SetPaneText(2,s);
			pStatusBar->SetPaneWidth(2,s.GetLength()*8);
			
			pStatusBar->SetPaneText(3,sArea);
			pStatusBar->SetPaneWidth(3,sArea.GetLength()*8);
			
			pStatusBar->SetPaneText(4,sVolume);
			pStatusBar->SetPaneWidth(4,sVolume.GetLength()*8);
			
			pStatusBar->SetPaneText(5,sCentre);
			pStatusBar->SetPaneWidth(5,sCentre.GetLength()*8);
			
			pStatusBar->CanBeResized();

			LPCTSTR szTemp = (LPCTSTR)sFileName;
			(AfxGetMainWnd( ))->SetWindowText(szTemp);

		}
		
		void CModelViewerView::DeleteDisplayList()
		{
			if(m_uiDisplayListIndex_Point)
			{
				::glDeleteLists(m_uiDisplayListIndex_Point,1);
				m_uiDisplayListIndex_Point = 0;
			}
			if(m_uiDisplayListIndex_Line)
			{
				::glDeleteLists(m_uiDisplayListIndex_Line,1);
				m_uiDisplayListIndex_Line = 0;
			}
			if(m_uiDisplayListIndex_Face)
			{
				::glDeleteLists(m_uiDisplayListIndex_Face,1);
				m_uiDisplayListIndex_Face = 0;
			}
			if(m_uiDisplayListIndex_NormalLine)
			{
				::glDeleteLists(m_uiDisplayListIndex_NormalLine,1);
				m_uiDisplayListIndex_NormalLine= 0;
			}
			if(m_uiDisplayListIndex_FaceNormalLine)
			{
				::glDeleteLists(m_uiDisplayListIndex_FaceNormalLine,1);
				m_uiDisplayListIndex_FaceNormalLine= 0;
			}
			if(m_uiDisplayListIndex_curvature)
			{
				::glDeleteLists(m_uiDisplayListIndex_curvature,1);
				m_uiDisplayListIndex_curvature = 0;
			}
			
		}


		void CModelViewerView::OnFileNew()
		{
			// TODO: Add your command handler code here
		}


	