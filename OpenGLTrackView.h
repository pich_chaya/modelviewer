
// OpenGLTrackView.h : interface of the COpenGLTrackView class
//

#pragma once
#include "OpenGLBaseView.h"

//TrackingMode
enum ETRACKINGMODE
{
	TM_NONE = 0,
	TM_SPIN,
	TM_PAN,
	TM_ZOOM,
};

class COpenGLTrackView : public COpenGLBaseView
{
//member variables
private:
	
	CPoint			m_ptLast;
	GLfloat			m_f3LastPos[3];
	GLfloat			m_f3RenderingCenter[3];  //center of drawing
	GLfloat			m_fRenderingRate;		//rate
	GLfloat			m_objectXform[4][4];
protected:
	ETRACKINGMODE	m_eTrackingMode;
//member functions
private:
	void	BeginTracking(CPoint point, ETRACKINGMODE eTrackingMode);
	void	EndTracking();
	void	ptov(GLfloat p[2], GLfloat v[3], GLfloat fWidth, GLfloat fHeight);
	void	DoRotation(const float fAngle, const float fX, const float fY, const float fZ);
	bool	SetupViewingTransform();
protected:
	bool	SetupViewingFrustum();
	virtual void	DoTracking(CPoint point);
protected: // create from serialization only
	COpenGLTrackView();
	DECLARE_DYNCREATE(COpenGLTrackView)

// Attributes
public:
	CDocument* GetDocument() const;

// Operations
public:

// Overrides
public:
//	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:

// Implementation
public:
	virtual ~COpenGLTrackView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	afx_msg void OnFilePrintPreview();
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
};

#ifndef _DEBUG  // debug version in OpenGLTrackView.cpp
inline CDocument* COpenGLTrackView::GetDocument() const
   { return reinterpret_cast<CDocument*>(m_pDocument); }
#endif

