
// OpenGLBaseView.cpp : implementation of the COpenGLBaseView class
//

#include "stdafx.h"
// SHARED_HANDLERS can be defined in an ATL project implementing preview, thumbnail
// and search filter handlers and allows sharing of document code with that project.
#ifndef SHARED_HANDLERS
//#include "OpenGLBase.h"
#endif

//#include "OpenGLBaseDoc.h"
#include "OpenGLBaseView.h"

#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// COpenGLBaseView

IMPLEMENT_DYNCREATE(COpenGLBaseView, CView)

BEGIN_MESSAGE_MAP(COpenGLBaseView, CView)
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
	ON_WM_ERASEBKGND()
	ON_WM_SIZE()
	ON_WM_DESTROY()
END_MESSAGE_MAP()

// COpenGLBaseView construction/destruction

COpenGLBaseView::COpenGLBaseView():
	m_hRC(NULL),
	m_pDC(NULL),
	m_uiDisplayListIndex_StockScene(0),
	m_cx(0),
	m_cy(0)
{
	// TODO: add construction code here
}

bool COpenGLBaseView::InitializeOpenGL()
{
	m_pDC = new CClientDC(this);
	if(NULL == m_pDC)
	{
		ASSERT(!"m_pDC is NULL");
		return false;
	}

	//A setup of OpenGL pixel format
	if(!SetupPixelFormat())
	{
		ASSERT(!"SetupPixelFormat failed");
		return false;
	}

	//create of a rendering context
	if(0==(m_hRC=::wglCreateContext(m_pDC->GetSafeHdc())))
	{
		ASSERT(!"wglCreateContext failed");
		return false;
	}

	//A rencering context is set as a current device context
	if(TRUE!=::wglMakeCurrent(m_pDC->GetSafeHdc(),m_hRC))
	{
		ASSERT(!"wglMakeCurrent failed");
		return false;
	}

	//A set up of ClrearColor
	::glClearColor(0,0,0,0); //black

	//clear of a depth buffer
	::glClearDepth(1.0f);

	//Depth test
	::glEnable(GL_DEPTH_TEST);  //depth test

	//Depth func.
	::glDepthFunc(GL_LEQUAL);

	::glEnable(GL_NORMALIZE);

	return true;
}

bool COpenGLBaseView::SetupPixelFormat()
{
	static PIXELFORMATDESCRIPTOR pfd =
	{
		sizeof(PIXELFORMATDESCRIPTOR),1,PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER, PFD_TYPE_RGBA,
		24,
		0,0,0,0,0,0,
		0,
		0,
		0,
		0,0,0,0,
		32,
		0,
		0,
		PFD_MAIN_PLANE,
		0,
		0,0,0
	};

	int iPixelFormat;
	if(0==(iPixelFormat = ::ChoosePixelFormat(m_pDC->GetSafeHdc(),&pfd)))
	{
		ASSERT(!"ChoosePixelFormat is failed");
		return false;
	}

	if(TRUE !=SetPixelFormat(m_pDC->GetSafeHdc(),iPixelFormat,&pfd))
	{
		ASSERT(!"SetPixelFormat is failed");
		return false;
	}

	return true;

}

void COpenGLBaseView::UninitializeOpenGL()
{
	if(m_hRC)
	{
		::wglDeleteContext(m_hRC);
		m_hRC = NULL;
	}
	if(m_pDC)
	{
		delete m_pDC;
		m_pDC = NULL;
	}
}

bool COpenGLBaseView::SetupViewport()
{
	::glViewport(0,0,m_cx,m_cy);
	return true;
}

bool COpenGLBaseView::SetupViewingFrustum()
{
	::glOrtho(-m_cx*0.5/10.0,m_cx*0.5/10.0,-m_cy*0.5/10.0,m_cy*0.5/10.0,0.1,1000.0);
	return true;
}

bool COpenGLBaseView::SetupViewingTransform()
{
	gluLookAt(0.0,0.0,500.0 //viewpoint
		,0.0,.0,0.0  //gaze-point
		,0.0,1.0,0.0); //up vector
	return true;
}

void COpenGLBaseView::RenderStockScene()
{
	if(0==m_uiDisplayListIndex_StockScene)
	{
		//Drawing Axis
		m_uiDisplayListIndex_StockScene = ::glGenLists(1);
		if(0 == m_uiDisplayListIndex_StockScene)
		{
			return;
		}
		::glNewList(m_uiDisplayListIndex_StockScene,GL_COMPILE);
		RenderAxis();
		::glEndList();
	}

	if(0 != m_uiDisplayListIndex_StockScene)
	{
		::glCallList(m_uiDisplayListIndex_StockScene);
	}
	
}

void COpenGLBaseView::RenderAxis()
{
	float	point[4][3];
	point[0][0] = 0.0;
	point[0][1] = 0.0;
	point[0][2] = 0.0;
	point[1][0] = 10;
	point[1][1] = 0.0;
	point[1][2] = 0.0;
	point[2][0] = 0.0;
	point[2][1] = 10;
	point[2][2] = 0.0;
	point[3][0] = 0.0;
	point[3][1] = 0.0;
	point[3][2] = 10;

	::glPushAttrib(GL_CURRENT_BIT|GL_LINE_BIT); //color | line information
	::glLineWidth(2.0);
	::glBegin(GL_LINES);
	//line X
		::glColor3ub(255,0,0);
		::glVertex3fv(point[0]);
		::glVertex3fv(point[1]);
	//line Y
		::glColor3ub(0,255,0);
		::glVertex3fv(point[0]);
		::glVertex3fv(point[2]);
	
	//line Z
		::glColor3ub(0,0,255);
		::glVertex3fv(point[0]);
		::glVertex3fv(point[3]);
	::glEnd();
		::glPopAttrib();
}


COpenGLBaseView::~COpenGLBaseView()
{
}

BOOL COpenGLBaseView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	cs.style|=WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
	return CView::PreCreateWindow(cs);
}

// COpenGLBaseView drawing

void COpenGLBaseView::OnDraw(CDC* /*pDC*/)
{
	CDocument* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;
	
	// TODO: add draw code for native data here
	if(!m_pDC)
	{
		return;
	}
	::wglMakeCurrent(m_pDC->GetSafeHdc(),m_hRC);
	::glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	PreRenderScene();
	::glPushMatrix();
		RenderStockScene();
	::glPopMatrix();
	::glPushMatrix();
		RenderScene();
	::glPopMatrix();

	PostRenderScene();
	::glFinish();
	::SwapBuffers(m_pDC->GetSafeHdc());
}

void COpenGLBaseView::OnRButtonUp(UINT /* nFlags */, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void COpenGLBaseView::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
//	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}


// COpenGLBaseView diagnostics

#ifdef _DEBUG
void COpenGLBaseView::AssertValid() const
{
	CView::AssertValid();
}

void COpenGLBaseView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CDocument* COpenGLBaseView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CDocument)));
	return (CDocument*)m_pDocument;
}
#endif //_DEBUG


// COpenGLBaseView message handlers


BOOL COpenGLBaseView::OnEraseBkgnd(CDC* pDC)
{
	// TODO: Add your message handler code here and/or call default

	//return CView::OnEraseBkgnd(pDC);
	return TRUE;
}


void COpenGLBaseView::OnSize(UINT nType, int cx, int cy)
{
	CView::OnSize(nType, cx, cy);
	CView::OnSize(nType, cx,cy);

	if(0>=cx || 0>=cy)
	{
		return;
	}

	if(NULL==m_hRC)
	{
		//InitializeOpenGL
		InitializeOpenGL();

	}

	if(NULL!=m_hRC)
	{
		//set up screen
		m_cx = cx;
		m_cy = cy;

		//set up viewport
		SetupViewport();

		//Setup of viewing frustum
		::glMatrixMode(GL_PROJECTION);
		::glLoadIdentity();
		SetupViewingFrustum();
		
		//Gaze0point transformation
		::glMatrixMode(GL_MODELVIEW);
		::glLoadIdentity();
		SetupViewingTransform();
	}
	// TODO: Add your message handler code here
}


void COpenGLBaseView::OnDestroy()
{
	CView::OnDestroy();
	UninitializeOpenGL();
	// TODO: Add your message handler code here
}

void  doResize(int newWidth, int newHeight)
{

  GLfloat aspectRatio = (GLfloat)newWidth / (GLfloat)newHeight;
  glViewport(0, 0, newWidth, newHeight);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(60.0, aspectRatio, 0.1, 40.0);
 
  
  /* WARNING: matrix mode left as projection! */

}
