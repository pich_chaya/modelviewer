// LoadPlyFile.h
//
#pragma once

#include "ModelDataStructure.h"

bool LoadPlyFile(const char* pszFileName, CModel& model);