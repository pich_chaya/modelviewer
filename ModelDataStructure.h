//
// ModelDataStructure.h
//
#pragma once
#include "Vec.h"
#include "lineqn.h"
#include <vector>

using namespace trimesh;

// i+1 and i-1 modulo 3
// This way of computing it tends to be faster than using %
#define NEXT(i) ((i)<2 ? (i)+1 : (i)-2)
#define PREV(i) ((i)>0 ? (i)-1 : (i)+2)

class CModel
{
public:
	///////Model Controller///////
	std::vector<vec>		vVertex;				// Vertex Array
	std::vector<vec>		vIndexedTriangle;		//Triangle Array
	std::vector<vec>		vVertexNormal;			
	std::vector<vec>		vFacesNormal;       
	float					volume;
	float					totalArea;
	vec						centerOfGravity;
	vec						center; 
	std::vector<float>		curvature;				//curvature
	std::vector<std::vector<unsigned int>> connectivity;

	//Face Normal
	void FaceNormal()
	{
		vec p1,p2,p3;
		vec normalVector;

		vFacesNormal.resize(vIndexedTriangle.size());

		for(int i=0;i<vIndexedTriangle.size();i++)
		{
			//vector p1
			p1 = vVertex[vIndexedTriangle[i][0]];

			//vector p2
			p2 = vVertex[vIndexedTriangle[i][1]];

			//vector p3
			p3 = vVertex[vIndexedTriangle[i][2]];

			//outer product
			normalVector = (p2-p1) CROSS (p3-p1);

			//Normalize 
			//add to vFaceNormal
			vFacesNormal[i]=normalize(normalVector);
		}

	}

	//Vertex Normal
	void VertexNormal()
	{
		//vertex v1, v2, v3, ....
		//triangle tr1, tr2, tr3 // all share vertex v1
		//v1.normal = normalize( tr1.normal + tr2.normal + tr3.normal )

		vVertexNormal.resize(vVertex.size());

		for(int i=0;i<vVertex.size();i++)
		{
			vec n;
			for(int j=0;j<connectivity[i].size();j++)
			{
				n += vFacesNormal[connectivity[i][j]];
			}

			vVertexNormal[i] = normalize(n);
		}
	}

};

