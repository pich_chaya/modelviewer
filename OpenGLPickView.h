
// OpenGLPickView.h : interface of the COpenGLPickView class
//

#pragma once
#include "OpenGLTrackView.h"

//size of name array
#define NAMEARRAYSIZE (10)

//Render Mode
enum ERENDERMODE
{
	RM_UNDEFINE = 0,
	RM_RENDER,	//normal mode
	RM_PICK,	//Pick drawing
	RM_SELECT,	//Determine processing what was chosen
};

class COpenGLPickView : public COpenGLTrackView
{
//member variable
private:
protected:
	unsigned int  m_auiName_pick[NAMEARRAYSIZE];

//member functions
private:
	bool  RenderScene();
	
	void SelectionBuffer2NameArray(GLuint uiCountHit, GLuint uiSelectionBuffer[], unsigned int uiNameArray_dest[NAMEARRAYSIZE]);
protected:
	virtual void  RenderObjects(ERENDERMODE eRenderMode, const unsigned int* auiName);
	int DoPicking(int x, int y);

protected: // create from serialization only
	COpenGLPickView();
	DECLARE_DYNCREATE(COpenGLPickView)

// Attributes
public:
	CDocument* GetDocument() const;

// Operations
public:

// Overrides
public:
//	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:

// Implementation
public:
	virtual ~COpenGLPickView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	afx_msg void OnFilePrintPreview();
//	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
};

#ifndef _DEBUG  // debug version in OpenGLPickView.cpp
inline CDocument* COpenGLPickView::GetDocument() const
   { return reinterpret_cast<CDocument*>(m_pDocument); }
#endif

