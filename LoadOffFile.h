// LoadOffFile.h
//
#pragma once

#include "ModelDataStructure.h"

bool LoadOffFile(const char* pszFileName, CModel& model);
