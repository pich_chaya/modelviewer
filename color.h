#include "stdafx.h"
#include "vec.h"
using namespace trimesh;

vec HSVtoRGB(vec hsv) {
	int Hi;
	float f, p, q, t;
	vec rgb;

	hsv[0]*=360.0;

	Hi = ((int)(hsv[0] / 60)) % 6;
	f = hsv[0] / 60 - Hi;
	p = hsv[2] * (1 - hsv[1]);
	q = hsv[2] * (1 - f * hsv[1]);
	t = hsv[2] * (1 - (1 - f) * hsv[1]);

	switch (Hi) {
	case 0: rgb[0] = hsv[2]; rgb[1] = t; rgb[2] = p; break;
	case 1: rgb[0] = q; rgb[1] = hsv[2]; rgb[2] = p; break;
	case 2: rgb[0] = p; rgb[1] = hsv[2]; rgb[2] = t; break;
	case 3: rgb[0] = p; rgb[1] = q; rgb[2] = hsv[2]; break;
	case 4: rgb[0] = t; rgb[1] = p; rgb[2] = hsv[2]; break;
	case 5: rgb[0] = hsv[2]; rgb[1] = p; rgb[2] = q; break;
	}

	return rgb;
}

vec RGBtoHSV(vec rgb) {
	float max;
	float min;
	float R, G, B;
	vec hsv;

	R = rgb[0];
	G = rgb[1];
	B = rgb[2];

	// max min
	if (R >= G && R >= B) {
		max = R;
		min = (G < B) ? G : B;
	} else if (G >= R && G >= B) {
		max = G;
		min = (R < B) ? R : B;
	} else {
		max = B;
		min = (R < G) ? R : G;
	}

	// Hue
	if (max == R) {
		hsv[0] = 60 * (G - B) / (max - min);
	} else if (max == G){
		hsv[0] = 60 * (B - R) / (max - min) + 120;
	} else {
		hsv[0] = 60 * (R - G) / (max - min) + 240;
	}
	if (hsv[0] < 0.0) {
		hsv[0] += 360.0;
	}

	hsv[0]/=360.0;

	// Saturation
	hsv[1] = (max - min) / max;
	// Value
	hsv[2] = max;

	return hsv;
}